/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.vimtips.supacount;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class CountDownProvider extends ContentProvider {
	private SQLiteOpenHelper helper;
	private static final int COUNTDOWNS = 1;
	private static final int COUNTDOWN_ID = 2;
	private static final UriMatcher sUrlMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	
	private static final String TAG = "CountDownProvider";
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);
	
	static {
		sUrlMatcher.addURI("org.vimtips.supacount", "countdown", COUNTDOWNS);
		sUrlMatcher.addURI("org.vimtips.supacount", "countdown/#", COUNTDOWN_ID);
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
		private static final String DATABASE_NAME = "countdowns.db";
		private static final int DATABASE_VERSION = 1;
		
		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE counts(" +
					"_id INTEGER PRIMARY KEY, " +
					"description VARCHAR(255) NOT NULL, " +
					"seconds INTEGER UNSINGED NOT NULL, " +
					"time_started INTEGER UNSIGNED NOT NULL DEFAULT 0, " +
					"running INTEGER UNSIGNED NOT NULL DEFAULT 0, " +
					"time_left INTEGER UNSIGNED NOT NULL DEFAULT 0)");
			
			String insertMe = "INSERT INTO counts (description, " +
				"seconds) VALUES ";
			db.execSQL(insertMe + "(\"25 Minutes\", 1500)");
			db.execSQL(insertMe + "(\"30 Minutes\", 1800)");
			db.execSQL(insertMe + "(\"45 Minutes\", 2700)");
			db.execSQL(insertMe + "(\"1 Hour\", 3600)");
			db.execSQL(insertMe + "(\"10 Seconds\", 10)");
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// not implemented or needed yet
		}
	}
	
	public CountDownProvider() {
	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		SQLiteDatabase db = helper.getWritableDatabase();
		int count;
		
		@SuppressWarnings("unused")
		long rowId = 0;
		switch(sUrlMatcher.match(uri)) {
		case COUNTDOWNS:
			count = db.delete("counts", where, whereArgs);
			break;
		case COUNTDOWN_ID:
			String segment = uri.getPathSegments().get(1);
			rowId = Long.parseLong(segment);
			if(TextUtils.isEmpty(where)) {
				where = "_id=" + segment;
			}
			else {
				where = "_id=" + segment + " AND (" + where + ")";		
			}
			count = db.delete("counts", where, whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Cannot delete form URL: " + uri);
		}
		
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		int match = sUrlMatcher.match(uri);
		switch(match) {
		case COUNTDOWNS:
			return "vdn.android.cursor.dir/countdowns";
		case COUNTDOWN_ID:
			return "vdn.android.cursor.item/countdowns";
		default:
			throw new IllegalArgumentException("Unknown URL");
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		if(sUrlMatcher.match(uri) != COUNTDOWNS) {
			throw new IllegalArgumentException("Cannot insert into url: " + uri);
		}
		
		ContentValues values;
		if(initialValues != null) {
			values = new ContentValues(initialValues);
		}
		else {
			values = new ContentValues();
		}
		
		if(!values.containsKey(CountDown.Columns.DESCRIPTION))
			values.put(CountDown.Columns.DESCRIPTION, "");
		
		if(!values.containsKey(CountDown.Columns.SECONDS))
			values.put(CountDown.Columns.SECONDS, 10);
		
		if(!values.containsKey(CountDown.Columns.TIME_STARTED)) 
			values.put(CountDown.Columns.TIME_STARTED, 0);
		
		if(!values.containsKey(CountDown.Columns.RUNNING)) 
			values.put(CountDown.Columns.RUNNING, 0);
		
		if(!values.containsKey(CountDown.Columns.TIME_LEFT))
			values.put(CountDown.Columns.TIME_LEFT, 0);
		
		SQLiteDatabase db = helper.getWritableDatabase();
		long rowId = db.insert("counts", CountDown.Columns.DESCRIPTION, values);
		if(rowId <= 0) {
			throw new SQLException("Failed to insert row into " + uri);
		}
		
		Uri newUrl = ContentUris.withAppendedId(CountDown.Columns.CONTENT_URI, rowId);
		getContext().getContentResolver().notifyChange(uri, null);
		
		return newUrl;
	}

	@Override
	public boolean onCreate() {
		helper = new DatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		
		int match = sUrlMatcher.match(uri);
		switch(match) {
		case COUNTDOWNS:
			qb.setTables("counts");
			break;
		case COUNTDOWN_ID:
			qb.setTables("counts");
			qb.appendWhere("_id=");
			qb.appendWhere(uri.getPathSegments().get(1));
			break;
		default:
			throw new IllegalArgumentException("Unknown URL " + uri);
		}
		
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor ret = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		
		if(ret == null) {
			if(DEBUG) Log.d(TAG, "CountDowns Query failed");
		}
		else {
			ret.setNotificationUri(getContext().getContentResolver(), uri);
		}
		
		return ret;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		
		int count;
		long rowId = 0;
		int match = sUrlMatcher.match(uri);
		SQLiteDatabase db = helper.getWritableDatabase();
		switch(match) {
		case COUNTDOWNS:
			count = db.update("counts", values, null, null);
			break;
		case COUNTDOWN_ID:
			String segment = uri.getPathSegments().get(1);
			rowId = Long.parseLong(segment);
			count = db.update("counts", values, "_id=" + rowId, null);
			break;
		default:
			throw new UnsupportedOperationException("Cannot update url: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;		
	}

}
