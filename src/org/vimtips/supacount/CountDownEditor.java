/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vimtips.supacount;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.vimtips.supacount.CountDown;

/**
 * A Dialog for adding and editing CountDown objects
 */
public class CountDownEditor extends Activity {
	public static final int RESULT_SAVE = Activity.RESULT_FIRST_USER + 1;
	public static final int RESULT_SAVE_RUN = Activity.RESULT_FIRST_USER + 2;
	private EditText descriptionBox;
	private EditText hoursBox;
	private EditText minutesBox;
	private EditText secondsBox;
	private CountDown count;
	
	/**
	 * Sets up the dialog
	 */
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
		setContentView(R.layout.add_edit);
		descriptionBox = (EditText)findViewById(R.id.description_box);
		hoursBox = (EditText)findViewById(R.id.hours_box);
		minutesBox = (EditText)findViewById(R.id.minutes_box);
		secondsBox = (EditText)findViewById(R.id.seconds_box);
		setupEvents();
		setEmptyCount();
		
		// check to see if edit information was passed in
		Bundle bundle = this.getIntent().getExtras();
		if(bundle != null) count = bundle.getParcelable("countdown");
		
		if(count != null) {			
			setCountDown(count);
		}
		else {
			count = new CountDown("", 0);
		}
	}
	
	/**
	 * Binds events to the two buttons
	 */
	private void setupEvents()
	{
		Button saveButton = (Button)findViewById(R.id.save);
		Button saveRunButton = (Button)findViewById(R.id.save_run);
		
		saveButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				saveCount(false);
			}
		});
		
		saveRunButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				saveCount(true);
			}
		});
	}
	
	/**
	 * Shows an AlertDialog containing one neutral button, and the title "Save Countdown"
	 * 
	 * @param title: The title of the dialog
	 * @param message: The dialog message
	 */
	private void showAlertDialog(String message) {

		new AlertDialog.Builder(this)
			.setTitle(getString(R.string.save_countdown))
			.setMessage(message)
			.setNeutralButton(getString(R.string.close), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dlg, int result) {
					// pass
				}
			}).show();
	}
	
	/**
	 * Checks to see if the fields filled out in this dialog have valid values
	 * 
	 * @return true if they are valid
	 */
	private boolean validate() {
		if(descriptionBox.getText().toString().equals("")) {
			showAlertDialog(getString(R.string.enter_a_description));
			return false;
		}
		
		if(getSeconds() <= 0) {
			showAlertDialog(getString(R.string.enter_valid_time));
			return false;
		}
		
		return true;
	}
	
	/**
	 * Saves a CountDown
	 */
	private void saveCount(boolean run) {
		if(!validate()) return;
		Intent i = new Intent();
		
		count.setDescription(descriptionBox.getText().toString());
		count.setSeconds(getSeconds());
		i.putExtra("countdown", count);
		int result = RESULT_SAVE;
		if(run) result = RESULT_SAVE_RUN;
		setResult(result, i);
		finish();		
	}
	
	/**
	 * Sets the data of this dialog from the specified CountDown object
	 */
	public void setCountDown(CountDown count) {
		descriptionBox.setText(count.getDescription());
		
		long info[] = count.getHMS();
		
		hoursBox.setText(new Long(info[0]).toString());
		minutesBox.setText(new Long(info[1]).toString());
		secondsBox.setText(new Long(info[2]).toString());
	}
	
	/**
	 * Gets the number of seconds represented by this dialog.  
	 * 
	 * Finds the hours, minutes, seconds and computes the number of seconds.
	 */
	private long getSeconds() {
		long hours = getLongFromField(hoursBox);
		long minutes = getLongFromField(minutesBox);
		long secs = getLongFromField(secondsBox);
		
		long seconds = hours * 3600;
		seconds += minutes * 60;
		seconds += secs;
		
		return seconds;
	}
	
	/**
	 * Gets the Long value of the specified field, or 0 if it is empty
	 */
	private long getLongFromField(EditText field) {
		if(field.getText().toString().equals("")) return 0L;
		long val = Long.parseLong(field.getText().toString());
		if(val <=0 ) return 0L;
		return val;
	}
	
	/**
	 * Clears the dialog
	 */
	public void setEmptyCount() {
		descriptionBox.setText("");
		hoursBox.setText("00");
		minutesBox.setText("00");
		secondsBox.setText("00");
	}
}
