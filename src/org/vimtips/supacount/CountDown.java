/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.vimtips.supacount;

import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * The CountDown object.  Represents every count down that the user 
 * has created
 * @author Adam Olsen
 *
 */
public class CountDown implements Parcelable {
	
	public static class Columns implements BaseColumns {
		public static final Uri CONTENT_URI = 
			Uri.parse("content://org.vimtips.supacount/countdown");
		public static final String DESCRIPTION = "description";
		public static final String SECONDS = "seconds";
		public static final String TIME_STARTED = "time_started";
		public static final String RUNNING = "running";
		public static final String TIME_LEFT = "time_left";
		
		public static final String DEFAULT_SORT_ORDER = _ID + "ASC";
		static final String[] COUNTDOWN_QUERY_COLUMNS = {
				_ID, DESCRIPTION, SECONDS, TIME_STARTED, RUNNING, TIME_LEFT};
		
		public static final int COUNTDOWN_ID_INDEX = 0;
		public static final int COUNTDOWN_DESCRIPTION_INDEX = 1;
		public static final int COUNTDOWN_SECONDS_INDEX = 2;
		public static final int COUNTDOWN_TIME_STARTED_INDEX = 3;
		public static final int COUNTDOWN_RUNNING_INDEX = 4;
		public static final int COUNTDOWN_TIME_LEFT_INDEX = 5;
	}
	
	private static final String TAG = "CountDown";
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);
	private int id;
	private long seconds;
	private String description;
	private long timeStarted;
	private long timeLeft;
	private boolean running;
	
	/**
	 * Creates a CountDown object from a Cursor
	 */
	public CountDown(Cursor c) {
		id = c.getInt(Columns.COUNTDOWN_ID_INDEX);
		description = c.getString(Columns.COUNTDOWN_DESCRIPTION_INDEX);
		seconds = c.getLong(Columns.COUNTDOWN_SECONDS_INDEX);
		timeStarted = c.getLong(Columns.COUNTDOWN_TIME_STARTED_INDEX);
		running = c.getInt(Columns.COUNTDOWN_RUNNING_INDEX) > 0;
		timeLeft = c.getLong(Columns.COUNTDOWN_TIME_LEFT_INDEX);
	}
	
	/**
	 * Initializes the countdown with an id
	 * 
	 * @param id 			The id of the countdown
	 * @param description 	The description
	 * @param seconds 		Number of seconds the countdown will last.
	 */
	public CountDown(int id, String description, long seconds)
	{
		init(id, description, seconds);
	}
	
	/**
	 * Initializes the countdown without an id
	 * 
	 * @param description	The description
	 * @param seconds		Number of seconds the countdown will last
	 */
	public CountDown(String description, long seconds) {
		init(-1, description, seconds);
	}
	
	/**
	 * Initializes the countdown
	 * 
	 * @param id			The id
	 * @param description	The description
	 * @param seconds		Number of seconds
	 */
	protected void init(int id, String description, long seconds)
	{
		this.id = id;
		this.description = description;
		this.seconds = seconds;
		timeStarted = 0;
		running = false;
		timeLeft = seconds;
		
		if(DEBUG) {
			Log.d(TAG, "Initializing countdown: " + description);
		}
	}
	
	/**
	 * Determines if one CountDown is equal to another CountDown by value
	 */
	public boolean equals(CountDown count) {
		if(count.id != id) return false;
		else if(!count.description.equals(description)) return false;
		else if(count.seconds != seconds) return false;
		else if(count.timeLeft != timeLeft) return false;
		else if(count.timeStarted != timeStarted) return false;
		else if(count.running != running) return false;
		
		return false;
	}
	
	/**
	 * Starts this CountDown
	 * 
	 * Marks it as started, sets the time it started
	 */
	public void start() {
		running = true;
		timeStarted = System.currentTimeMillis();
	}
	
	/**
	 * Stops the countdown, sets running as false and the timeStarted to 0
	 */
	public void stop() {
		running = false;
		timeStarted = 0;
	}
	
	/**
	 * Parcelable interface
	 */
	public static final Parcelable.Creator<CountDown> CREATOR = new Parcelable.Creator<CountDown>() {
		/**
		 * Creates the countdown from a Parcel
		 */
		public CountDown createFromParcel(Parcel in) {
			return new CountDown(in);
		}
		
		/**
		 * Creates a new array of countdowns
		 */
		public CountDown[] newArray(int size) {
			return new CountDown[size];
		}
	};
	
	/**
	 * Parcel constructor
	 * @param in  The parcel to initialize the countdown
	 */
	private CountDown(Parcel in) {
		readFromParcel(in);
	}
	
	/**
	 * Writes the countdown to a parcel
	 * 
	 * @param out		The parcel to write the object to
	 * @param flags		Not used at this time
	 */
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeString(description);
		out.writeLong(seconds);
		out.writeLong(timeStarted);
		out.writeLong(timeLeft);
		out.writeInt(running ? 1 : 0);
	}
	
	/**
	 * Used by android, returns 0
	 */
	public int describeContents() { return 0; }
	
	/**
	 * Reads the countdown from a parcel
	 * @param in	The parcel
	 */
	public void readFromParcel(Parcel in) {
		id = in.readInt();
		description = in.readString();
		seconds = in.readLong();
		timeStarted = in.readLong();
		timeLeft = in.readLong();
		running = in.readInt() > 0 ? true : false;
	}
	
	/**
	 * Gets the countdown id
	 * 
	 * @return the id
	 */
	public int getId() 
	{
		// yes, I'm casting a long to an int here.  I'm not sure how else
		// to get around it, but Google does it in their AlarmClock application
		// as well.  Perhaps db.insert only returns the type long, but it never
		// gets to a value that's larger than an int.  I don't know.
		return (int)id;
	}
	
	/**
	 * Sets the id from an int
	 */
	protected void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Returns the seconds this countdown will last
	 * 
	 * @return the seconds
	 */
	public long getSeconds()
	{
		return seconds;
	}
	
	/**
	 * Gets the description of this countdown
	 * 
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}
	
	/**
	 * Sets the number of seconds this countdown will last
	 * 
	 * @param seconds the seconds
	 */
	public void setSeconds(long seconds)
	{
		this.seconds = seconds;
		this.running = false;
		this.timeStarted = 0;
		this.timeLeft = seconds;
	}
	
	/**
	 * Get the running status of this countdown
	 * @return true if the countdown is running, false if not
	 */
	public boolean isRunning() { return running; }
	
	/**
	 * Sets the countdown as running
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	/**
	 * Toggles pause
	 */
	public void togglePause() {
		if(timeStarted <= 0) {
			start();
			return;
		}
		
		if(running) {
			timeLeft = getSecondsLeft();
			running = false;		
		}
		else {
			running = true;
			long diff = (seconds - timeLeft) * 1000;
			timeStarted = System.currentTimeMillis() - diff;
		}
	}
	
	/**
	 * Sets the countdown description
	 * 
	 * @param description the description
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	/**
	 * Returns the formatted number of seconds
	 * 
	 * @return The seconds in HH:MM:SS format
	 */
	public String getSecondsFormatted()
	{
		return this.getFormatted(this.seconds);
	}
	
	
	/**
	 * Gets the number of seconds left formatted
	 * 
	 * @return  The number of seconds left in HH:MM:SS format
	 */
	public String getSecondsLeftFormatted()
	{
		return this.getFormatted(getSecondsLeft());
	}
	
	/**
	 * Gets the number of seconds left on this timer
	 */
	public long getSecondsLeft() {
		if(timeStarted <= 0) { return seconds; }
		if(!running) { return timeLeft; }
		return seconds - ((System.currentTimeMillis() - timeStarted) / 1000);
	}
	
	/**
	 * Gets the number of seconds in HH:MM:SS format
	 * 
	 * @param seconds  The seconds to format
	 * @return	The seconds in HH:MM:SS format
	 */
	private String getFormatted(long seconds) {		
		long info[] = getHoursMinutesSeconds(seconds);
		long hours = info[0];
		long minutes = info[1];
		long secs = info[2];
		
		return ((hours < 10 ? "0":"") + hours
					+ ":" + (minutes < 10 ? "0":"") + minutes
					+ ":" + (secs < 10 ? "0":"") + secs);
	}
	
	/**
	 * Gets the hours, minutes, and seconds left
	 * 
	 * @return the hours, minutes, and seconds left in an array
	 */
	public long[] getHMSLeft() {
		return getHoursMinutesSeconds(getSecondsLeft());
	}
	
	/**
	 * Gets the hours, minutes, and seconds
	 * 
	 * @return  The hours, minutes, and seconds in an array
	 */
	public long[] getHMS() {
		return getHoursMinutesSeconds(seconds);
	}
	
	/**
	 * Generates the hours, minutes, and seconds from the specified seconds
	 * 
	 * @param seconds the number of seconds as an long
	 * @return a three element array containing the hours, minutes, and seconds 
	 */	
	private long[] getHoursMinutesSeconds(long seconds)
	{
		long hours = seconds / 3600;
		long minutes = (seconds / 60) - (hours * 60);
		long secs = seconds % 60;
		
		
		if(minutes == 60)  {
			if(hours > 1) hours += 1;
			minutes = 0;
		}
		
		if(seconds == 60) {
			if(minutes > 1) minutes += 1;
			seconds = 0;
		}		
		
		return new long[] {hours, minutes, secs};
	}
	
	/**
	 * Gets the timeStarted of this countdown
	 */
	public long getTimeStarted() {
		return timeStarted;
	}
	
	/**
	 * Sets the start time
	 */
	public void setStartTime(long time) {
		timeStarted = time;
	}
	
	/**
	 * Resets the countdown.
	 * 
	 * Sets secondsLeft to the value in seconds
	 */
	 public void reset()
	 {
		 running = false;
		 timeStarted = 0;
	 }
}
