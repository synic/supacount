/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vimtips.supacount;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Main SupaCount Activity
 * 
 * SupaCount is a multi-countdown timer.  You can save different countdowns with different
 * descriptions and times.  You can start any number of the saved countdowns.  Optionally,
 * the user can be notified once a countdown reaches 0.
 * 
 * @author Adam Olsen
 */
public class SupaCount extends ListActivity {

	private static final String TAG = "SupaCountMain";  // logger tag
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);
	private CountDownAdapter model;
	private final int ADD_EDIT = 0;
	private final int PREFERENCES = 1;
	private Timer timer;
	private CountDowns counts;
	
	/**
	 * Called by Android when the activity is created.  Loads the saved countdowns, sets up 
	 * the CountDownAdapter
	 */	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(DEBUG) {
        	Log.d(TAG, "Starting up...");
        }

        counts = new CountDowns(this);
       	model = new CountDownAdapter(this, counts);
        setListAdapter(model);
        
        timer = new Timer(false);
		timer.schedule(new SupaCountTimer(), 0, 1000L);
                
        setContentView(R.layout.main);
        registerForContextMenu(getListView());
                
        PreferenceManager.setDefaultValues(this, R.layout.preferences, false);
    }
	
	/**
	 * Called when the application is backgrounded, and always called before it
	 * is killed.  Saves current state.
	 */
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	/**
	 * Called when someone clicks on one of the countdowns in the list.
	 * 
	 * Here it toggles the running state of the CountDown the user clicked.
	 */	
	@Override
	public void onListItemClick(ListView parent, View v, int position, long _i) {

		CountDown count = model.getItem(position);
		counts.togglePause(count);
	}
	
	/**
	 * Called when the SupaCountMain Activity is destroyed.  Does come cleanup work
	 */
	@Override
	public void onDestroy()
	{	
		super.onDestroy();
	}

	/**
	 * Called by Android when someone long clicks on one of the CountDown items
	 * in the list
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, 
			ContextMenu.ContextMenuInfo menuInfo) {

		new MenuInflater(getApplication()).inflate(R.menu.main_context, menu);
		menu.setHeaderTitle(getString(R.string.countdown));
	}
	
	/**
	 * Called when one of the menu items in the context menu is selected
	 */
	@Override
	public boolean onContextItemSelected(MenuItem item) {

		
		// find out which item was selected
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		CountDown count = model.getItem(info.position);
		
		switch(item.getItemId())
		{
		case R.id.reset_countdown:
			counts.resetCountDown(count);
			return true;
		case R.id.edit_countdown:
			Intent i = new Intent(this, CountDownEditor.class);
			i.putExtra("countdown", count);
			startActivityForResult(i, ADD_EDIT);
			return true;
		case R.id.delete_countdown:
			count.stop();
			synchronized(model) {
				model.remove(count);
			}
			counts.deleteCountDown(count);
			return true;
		}
		return false;
	}
	
	/**
	 * Called by Android the first time the user presses the menu button in 
	 * this activity.  This inflates the menu from the xml file main_options.xml
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		new MenuInflater(getApplication()).inflate(R.menu.main_options, menu);
		return(super.onCreateOptionsMenu(menu));
	}
	
	/**
	 * Called when someone clicks on one of the menu items in the options menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch(item.getItemId())
		{
		case R.id.reset_all:
			counts.resetAll();
			return true;
		case R.id.settings:
			startActivityForResult(new Intent(this, SupaCountPreferences.class),
					PREFERENCES);
			return true;
		case R.id.add:
			Intent i = new Intent(this, CountDownEditor.class);
			startActivityForResult(i, ADD_EDIT);
			return(true);
		case R.id.stop_all:
			counts.pauseAll();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Called when any sub-activity returns a result.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// handle results returned by the AddEditActivity
		if(resultCode == CountDownEditor.RESULT_SAVE || resultCode == CountDownEditor.RESULT_SAVE_RUN) {
			Bundle extras = intent.getExtras();
			CountDown count = extras.getParcelable("countdown");
			counts.saveCountDown(count);
			
			// check to see if the user specified to run this countdown after saving it
			if(resultCode == CountDownEditor.RESULT_SAVE_RUN) {
				counts.startCountDown(count);
			}
		}
		else if(resultCode == SupaCountPreferences.RESULT_DELETE_ALL) {
			counts.deleteAll();
		}
		
		model.notifyDataSetChanged();
	}

	
	/**
	 * The CountDownTimer.  Runs every second to countdown timeouts.  
	 * 
	 * @author Adam Olsen
	 */
	class SupaCountTimer extends TimerTask {	
		/**
		 * Called by the Timer object when this timertask should start running
		 */
		public void run()
		{			
			boolean refresh = false;
			
			synchronized(model) {
				for(CountDown count:model) {
					if(count.isRunning()) 
					{
						refresh = true;
						if(count.getSecondsLeft() <= 0) {
							count.stop();
						}
					}
				}
			}
			
			if(refresh) {
				runOnUiThread(new Runnable() {
					public void run() {
						model.notifyDataSetChanged();
					}
				});
			}
		}
	}	
}