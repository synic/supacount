/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vimtips.supacount;

import java.lang.reflect.Field;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * Alarm Clock alarm alert: pops visible indicator and plays alarm
 * tone
 */
public class AlarmAlert extends Activity {
	private static final String TAG = "AlarmAlert";
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);

    // These defaults must match the values in res/xml/settings.xml
    private static final String DEFAULT_VOLUME_BEHAVIOR = "2";

    private CountDown mCount;
    private int mVolumeBehavior;
    private KeyguardManager mKeyguardManager;
    private KeyguardManager.KeyguardLock mKeyguardLock;    

    // Receives the ALARM_KILLED action from the AlarmKlaxon.
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CountDown count = intent.getParcelableExtra(CountDowns.COUNTDOWN_INTENT_EXTRA);
            if (mCount.getId() == count.getId()) {
                dismiss(true);
            }
        }
    }; 

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        mCount = getIntent().getParcelableExtra(CountDowns.COUNTDOWN_INTENT_EXTRA);

        // Get the volume/camera button behavior setting
        final String vol =
                PreferenceManager.getDefaultSharedPreferences(this)
                .getString(SupaCountPreferences.KEY_VOLUME_BEHAVIOR,
                        DEFAULT_VOLUME_BEHAVIOR);
        mVolumeBehavior = Integer.parseInt(vol);

        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        
        // check for turn screen on flags, and only add them if they are available
        int flags = getFlagsForVersion();
        
        getWindow().addFlags(flags);
        mKeyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);        
        
        updateLayout();

        // Register to get the alarm killed intent.
        registerReceiver(mReceiver, new IntentFilter(CountDowns.ALARM_KILLED));
    }
    
    /**
     * Use reflection to add only the flags that are available in the 
     * current version of Android, starting at Android 1.5+
     * 
     * @return the flags available to the current platform
     */
    private int getFlagsForVersion() {
    	final String possibleFlags[] = new String[] {
    		"FLAG_SHOW_WHEN_LOCKED",
    		"FLAG_DISMISS_KEYGUARD",
    		"FLAG_TURN_SCREEN_ON"
    	};
    	
    	int flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
    	for(String flag:possibleFlags) {
    		try {
    			Field field = WindowManager.LayoutParams.class.getField(flag);
    			int value = field.getInt(null);
    			flags |= value;
    		}
    		catch(NoSuchFieldException e) { }
    		catch(IllegalAccessException e) { }
    	}
    	
    	return flags;
    }
    
    private synchronized void enableKeyguard() {
        if (mKeyguardLock != null) {
            mKeyguardLock.reenableKeyguard();
            mKeyguardLock = null;
        }
    }

    private synchronized void disableKeyguard() {
        if (mKeyguardLock == null) {
            mKeyguardLock = mKeyguardManager.newKeyguardLock(TAG);
            mKeyguardLock.disableKeyguard();
        }
    }    
    
    private void setTitle() {
        String label = mCount.getDescription();
        TextView title = (TextView) findViewById(R.id.alertTitle);
        title.setText(label);
    }

    // This method is overwritten in AlarmAlertFullScreen in order to show a
    // full activity with the wallpaper as the background.
    protected View inflateView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.alarm_alert, null);
    }

    private void updateLayout() {
        LayoutInflater inflater = LayoutInflater.from(this);

        setContentView(inflateView(inflater));

        /* dismiss button: close notification */
        findViewById(R.id.dismiss).setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        dismiss(false);
                    }
                });

        /* Set the title from the passed in alarm */
        setTitle();
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    // Dismiss the alarm.
    private void dismiss(boolean killed) {
        // The service told us that the alarm has been killed, do not modify
        // the notification or stop the service.
        if (!killed) {
            // Cancel the notification and stop playing the alarm
            NotificationManager nm = getNotificationManager();
            
            // hopeful casting to int doesn't end up bad here...
            nm.cancel(mCount.getId());
            stopService(new Intent(CountDowns.COUNTDOWN_ALERT_ACTION));
        }
        finish();
        enableKeyguard();
    }

    /**
     * this is called when a second alarm is triggered while a
     * previous alert window is still active.
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if(DEBUG) Log.v(TAG, "AlarmAlert.OnNewIntent()");

        mCount = intent.getParcelableExtra(CountDowns.COUNTDOWN_INTENT_EXTRA);

        setTitle();
        disableKeyguard();
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	disableKeyguard();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Don't hang around.
        finish();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(DEBUG) Log.v(TAG, "AlarmAlert.onDestroy()");
        // No longer care about the alarm being killed.
        unregisterReceiver(mReceiver);
        enableKeyguard();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // Do this on key down to handle a few of the system keys.
        boolean up = event.getAction() == KeyEvent.ACTION_UP;
        switch (event.getKeyCode()) {
            // Volume keys and camera keys dismiss the alarm
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_CAMERA:
            case KeyEvent.KEYCODE_FOCUS:
                if (up) {
                    switch (mVolumeBehavior) {
                    	case 1:
                            dismiss(false);
                            break;

                        default:
                            break;
                    }
                }
                return true;
            default:
                break;
        }
        return super.dispatchKeyEvent(event);
    }
}
