/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.vimtips.supacount;

import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.util.Log;


/**
 * Database abstraction for SupaCount
 * 
 * TODO: fix this.
 * 
 * I realize that this whole class is kinda hacky.  I couldn't think of a 
 * better way to do it and keep speed up and memory usage down.  Ideally,
 * I'd just register a ContentObserver and look for any changes at all to notify
 * the CountDownAdapter to update it's contents, and to schedule the next alarm.  
 * However, ContentObserver is only notified that there is a change, not *what* 
 * that change is.  As a result, the ContentObserver would need to do it's own
 * work to find out what items changed (or were added/removed), and this can be
 * horribly slow. Also, scheduling the next alarm cannot efficiently be implemented 
 * in such a ContentObserver, because each call to it triggers a loop through all 
 * CountDown objects in the database, which would be inefficient, as the 
 * ContentObserver is notified of *every* change to the ContentResolver.
 * 
 * @author Adam Olsen
 *
 */
public class CountDowns {
	public final static String CLEAR_NOTIFICATION = "clear_notification";
	public final static String COUNTDOWN_ALERT_ACTION = 
			"org.vimtips.supacount.COUNTDOWN_ALERT";
	public final static String ALARM_KILLED = "org.vimtips.supacount.COUNTDOWN_KILLED";
	
	public final static String COUNTDOWN_INTENT_EXTRA = "intent.extra.countdown";
	public final static String COUNTDOWN_RAW_DATA = "intent.extra.countdown_raw";
	
	public final static String COUNTDOWN_ID = "countdown_id";	
	private static final String TAG = "CountDowns";	
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);

	private Context context;
	private ArrayList<Observer> observers = new ArrayList<Observer>();	
	
	public CountDowns(Context context) {
		this.context = context;
	}
	
	public void addObserver(Observer observer) {
		synchronized(this) {
			observers.add(observer);
		}
	}
	
	public void removeObserver(Observer observer) {
		synchronized(this) {
			observers.remove(observer);
		}
	}

	/**
	 * Truncates the database
	 */
	public void deleteAll() {
		ContentResolver resolver = context.getContentResolver();
		resolver.delete(CountDown.Columns.CONTENT_URI, null, null);
		notifyReloadAll();
		cancelAlarm(context);
	}
	
	/**
	 * Starts a CountDown
	 */
	public void startCountDown(CountDown count) {
		count.start();
		saveCountDown(context, count);
		notifyCountChanged(count);
		scheduleNextAlarm(context);
	}
	
	/**
	 * Pauses all CountDowns
	 *
	 */
	public void pauseAll() {
		synchronized(this) {			
			CountDown[] counts = getCountDowns();
			for(CountDown count:counts) {
				if(count.isRunning()) {
					count.togglePause();
					saveCountDown(context, count);
				}
			}
			
		}
		notifyReloadAll();
		cancelAlarm(context);
	}
	
	/**
	 * Resets all CountDowns
	 */
	public void resetAll() {
		ContentResolver resolver = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put(CountDown.Columns.RUNNING, 0);
		values.put(CountDown.Columns.TIME_STARTED, 0);
		resolver.update(CountDown.Columns.CONTENT_URI, values, null, null);		
		notifyReloadAll();
		cancelAlarm(context);
	}
	
	/**
	 * Pauses a CountDown
	 */
	public void togglePause(CountDown count) {
		count.togglePause();
		saveCountDown(context, count);
		notifyCountChanged(count);
		scheduleNextAlarm(context);
	}
	
	/**
	 * Resets a CountDown
	 */
	public void resetCountDown(CountDown count) {
		count.reset();
		saveCountDown(context, count);
		notifyCountChanged(count);
		scheduleNextAlarm(context);
	}
	
	public void saveCountDown(CountDown count) {
		boolean newCount = count.getId() < 0;
		saveCountDown(context, count);
		notifyCountChanged(count);
		if(newCount) scheduleNextAlarm(context);
	}

	
	/**
	 * Saves a CountDown
	 */
	public static void saveCountDown(Context context, CountDown count) {
		
		/**
		 * If count.getId() == -1, it's a new count, so add one first, and then save it
		 */
		if(count.getId() == -1) {
			if(DEBUG) Log.d(TAG, "adding new countdown");
			Uri uri = addCountDown(context);
			String segment = uri.getPathSegments().get(1);
			Integer countId = Integer.parseInt(segment);
			count.setId(countId);
		}
		
		ContentResolver resolver = context.getContentResolver();
		ContentValues values = new ContentValues();
		
		values.put(CountDown.Columns.DESCRIPTION, count.getDescription());
		values.put(CountDown.Columns.SECONDS, count.getSeconds());
		values.put(CountDown.Columns.TIME_STARTED, count.getTimeStarted());
		values.put(CountDown.Columns.RUNNING, count.isRunning() ? 1 : 0);
		values.put(CountDown.Columns.TIME_LEFT, count.getSecondsLeft());
		
		resolver.update(ContentUris.withAppendedId(CountDown.Columns.CONTENT_URI, 
				count.getId()), values, null, null);
	}
	
	/**
	 * Determines the countdown that will alarm next
	 * @return the CountDown with the nearest countdown timeout
	 */
	private static CountDown nearestCountTimeout(Context context) {
		CountDown least = null;
		CountDown[] counts = getCountDowns(context);
		for(CountDown count:counts) {
			if(count.isRunning()) {
				if(least == null || count.getSecondsLeft() < least.getSecondsLeft()) {
					least = count;
				}
			}
		}
	
		return least;
	}
	
	/**
	 * Uses the AlarmManager to schedule an alarm for the nearest count timeout
	 */
	protected static void scheduleNextAlarm(Context context) {
		CountDown count = nearestCountTimeout(context);
		AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, AlarmReceiver.class);
		if(count != null) 
		{	
			Parcel out = Parcel.obtain();
			count.writeToParcel(out, 0);
			out.setDataPosition(0);
			i.putExtra(COUNTDOWN_RAW_DATA, out.marshall());
		}
		
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
		
		if(count != null) {
			if(DEBUG) Log.d(TAG, "Setting alarm for: " + count.getDescription());
			mgr.set(AlarmManager.RTC_WAKEUP, 
					System.currentTimeMillis() + (count.getSecondsLeft() * 1000), pi);
			
		}
		else {
			if(DEBUG) Log.d(TAG, "No alarms set");
			mgr.cancel(pi);
		}
	}	
	
	/**
	 * Cancels the PendingIntent in the AlarmManager
	 * @param context
	 */
	protected static void cancelAlarm(Context context) {
		if(DEBUG) Log.d(TAG, "Cancelling all alarms in AlarmManager");
		AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, AlarmReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 
				0, i, PendingIntent.FLAG_CANCEL_CURRENT);
		mgr.cancel(pi);
	}
		
	/**
	 * Creates a new CountDown
	 */
	public static Uri addCountDown(Context context) {
		ContentResolver contentResolver = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put(CountDown.Columns.DESCRIPTION, "New CountDown Timer");
		return contentResolver.insert(CountDown.Columns.CONTENT_URI, values);
	}
	
	/**
	 * Removes an existing CountDown.  
	 */
	public void deleteCountDown(CountDown count) {
		ContentResolver resolver = context.getContentResolver();
		Uri uri = ContentUris.withAppendedId(CountDown.Columns.CONTENT_URI, count.getId());
		resolver.delete(uri, "", null);
		notifyCountDeleted(count);
	}
	
	/**
	 * Gets all CountDowns
	 */
	public CountDown[] getCountDowns() {
		return getCountDowns(context);
	}
		
	/**
	 * Statis version of getting all countdowns
	 */
	public static CountDown[] getCountDowns(Context context)
	{
		ContentResolver resolver = context.getContentResolver();
		ArrayList<CountDown> counts = new ArrayList<CountDown>();
		Cursor cursor = getCountDownCursor(resolver);
		
		if(cursor.moveToFirst()) {
			while(!cursor.isAfterLast()) {
				int id = cursor.getInt(CountDown.Columns.COUNTDOWN_ID_INDEX);
				CountDown count = getCountDown(context, id);
				counts.add(count);
				cursor.moveToNext();
			}
			
			cursor.close();
		}
		
		return (CountDown[])counts.toArray(new CountDown[counts.size()]);
	}
	
	/**
	 * Queries all CountDowns
	 */
	public static Cursor getCountDownCursor(ContentResolver resolver) {
		return resolver.query(CountDown.Columns.CONTENT_URI, 
				CountDown.Columns.COUNTDOWN_QUERY_COLUMNS,
				null, null, null);
	}
	
	public static CountDown getCountDown(Context context, int id) {
		CountDown count = null;
		ContentResolver resolver = context.getContentResolver();
		
		Cursor cursor = resolver.query(ContentUris.withAppendedId(CountDown.Columns.CONTENT_URI, id),
				CountDown.Columns.COUNTDOWN_QUERY_COLUMNS,
				null, null, null);
		if(cursor != null) {
			if(cursor.moveToFirst()) {
				count = new CountDown(cursor);
			}
			cursor.close();
		}
		
		return count;
	}
	
	private synchronized void notifyReloadAll() {
		for(Observer ob:observers) ob.reloadAll();
	}
	
	private synchronized void notifyCountChanged(CountDown count) {
		for(Observer ob:observers) ob.countChanged(count);
	}
	
	private synchronized void notifyCountDeleted(CountDown count) {
		for(Observer ob:observers) ob.countDeleted(count);
	}
	
	public interface Observer {
		public void reloadAll();
		public void countChanged(CountDown count);
		public void countDeleted(CountDown count);
	}
}
