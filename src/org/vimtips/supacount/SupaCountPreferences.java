/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.vimtips.supacount;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceClickListener;

/**
 * The Preference Dialog generator
 * @author Adam Olsen
 *
 */
public class SupaCountPreferences extends PreferenceActivity {
	public static final String KEY_VOLUME_BEHAVIOR = "volume_button_setting";
	public static final String KEY_LED = "led";
	public static final String KEY_RINGTONE = "sound";
	public static final String KEY_VIBRATE = "vibrate";
	public static final int RESULT_DELETE_ALL = Activity.RESULT_FIRST_USER + 3;
	public static final String DEFAULT_ALARM = "content://settings/system/alarm_alert";
	public static final boolean DEFAULT_VIBRATE = true;
	public static final boolean DEFAULT_LED = true;

	/** 
	 * Called by Android when this activity is launched
	 */
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		
		addPreferencesFromResource(R.layout.preferences);

		Preference deleteAll = (Preference)findPreference("delete_all");
		deleteAll.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference p) {
				new AlertDialog.Builder(SupaCountPreferences.this)
					.setTitle(getString(R.string.delete_all))
					.setMessage(getString(R.string.delete_sure))
					.setNegativeButton(getString(R.string.no), null)
					.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dlg, int result) {
							Intent i = new Intent();
							setResult(RESULT_DELETE_ALL, i);
							finish();
						}
					}).show();
				return true;
			}
		});
	}
}
