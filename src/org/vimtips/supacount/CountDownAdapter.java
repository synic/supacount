/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.vimtips.supacount;

import java.util.Iterator;

import android.app.Activity;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * The model for the SupaCountActivity ListView
 */
class CountDownAdapter extends ArrayAdapter<CountDown> implements Iterable<CountDown>
{
	private Activity context;
	private static final String TAG = "CountDownAdapter";
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG) || true;
	private CountDowns counts;
	
	private CountDowns.Observer observer = new CountDowns.Observer() {
		public void countChanged(CountDown count) {
			
			if(DEBUG) {
				Log.d(TAG, "countChanged() called for: " + count.getDescription());
			}
			
			boolean found = false;
			int size = getCount();
			for(int i = 0; i < size; i++) {
				CountDown check = getItem(i);
				if(check.getId() == count.getId()) {
					remove(check);
					insert(count, i);
					found = true;
					break;
				}
			}
			
			if(!found) {
				add(count);
			}
		}
		
		public void reloadAll() {
			loadItems();
		}
		
		public void countDeleted(CountDown count) {
			int size = getCount();
			for(int i = 0; i < size; i++) {
				CountDown check = getItem(i);
				if(check.getId() == count.getId()) {
					remove(check);
					break;
				}
			}
		}
	};
	
	/**
	 * Sets up the model
	 */
	public CountDownAdapter(Activity context, CountDowns counts) {
		super(context, R.layout.row);
		this.context = context;
		this.counts = counts;
		counts.addObserver(observer);
		loadItems();
	}
	
	/**
	 * Clears the model, and sets it to the new array that was passed in
	 * 
	 * @param items: an array of CountDown instances
	 */
	public void loadItems() {
		clear();
		for(CountDown count:counts.getCountDowns()) {
			add(count);
		}
	}
	
	/**
	 * Returns a new iterator to implement iterable
	 */
	public Iterator<CountDown> iterator() {
		return new CountDownAdapterIterator();
	}
	
	/**
	 * Returns the View for a specific row.
	 * 
	 * In this case, it builds the view with the CountDown information and
	 * layout/row.xml
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		if(row == null) {
			LayoutInflater inflator = context.getLayoutInflater();
			row = inflator.inflate(R.layout.row, parent, false);
		}
		
		CountDown item = getItem(position);
		
		TextView description = (TextView)row.findViewById(R.id.count_desc);
		TextView secondsLeft = (TextView)row.findViewById(R.id.count_left);
		TextView seconds = (TextView)row.findViewById(R.id.count_time);

		int colorCode = R.color.stopped;
		if(item.isRunning()) colorCode = R.color.running;
		else {
			if(item.getSecondsLeft() != item.getSeconds()) {
				colorCode = R.color.paused;
			}
		}
		
		Resources r = context.getResources();
		secondsLeft.setTextColor(r.getColor(colorCode));
		
		description.setText(item.getDescription());
		secondsLeft.setText(item.getSecondsLeftFormatted());
		seconds.setText(item.getSecondsFormatted());
		
		return(row);
	}
	
	/**
	 * Iterator class, allows you to use a foreach loop on the CountDownAdapter
	 * @author synic
	 *
	 */
	public class CountDownAdapterIterator implements Iterator<CountDown> {
		int count;
		
		/**
		 * Initializes the iterator
		 */
		public CountDownAdapterIterator() {
			this.count = 0;
		}
		
		/**
		 * Returns the next item in the iterator
		 */
		public CountDown next() {
			CountDown item = getItem(count);
			count ++;
			return item;
		}
		
		/**
		 * Returns true if there is another item in the iterator
		 */
		public boolean hasNext() {
			return count < getCount();
		}
		
		/**
		 * Required for the Iterator interface, but not used.
		 */
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}