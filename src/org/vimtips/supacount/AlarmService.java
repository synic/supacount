/**
 * Copyright 2009 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// based heavily on Android's official AlarmClock application
package org.vimtips.supacount;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class AlarmService extends Service {
	private static final int ALARM_TIMEOUT_SECONDS = 10 * 60;
	private static final long [] vibratePattern = new long[] {500, 500};
	private static final String TAG = "AlarmService";
	private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);
	private boolean playing = false;
	private Vibrator vibrator;
	private MediaPlayer player;
	private CountDown count;
	private TelephonyManager phoneManager;
	private int initialCallState;
	private SharedPreferences prefs;
	
	private static final int KILLER = 1000;
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch(msg.what) {
			case KILLER:
				sendKillBroadcast((CountDown)msg.obj);
				stopSelf();
				break;
			}
		}
	};
	
	private PhoneStateListener phoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String ignored) {
			if(state != TelephonyManager.CALL_STATE_IDLE && state != initialCallState) {
				sendKillBroadcast(count);
				stopSelf();
			}
		}
	};
	
	@Override
	public void onCreate() {
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
		phoneManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		phoneManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
		AlarmAlertWakeLock.acquireCpuWakeLock(this);
	}

	@Override
	public void onDestroy() {
		stop();
		phoneManager.listen(phoneStateListener, 0);
		AlarmAlertWakeLock.releaseCpuLock();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		if(intent == null) {
			stopSelf();
			return;
		}
		
		final CountDown countExtra = intent.getParcelableExtra(
				CountDowns.COUNTDOWN_INTENT_EXTRA);
		if(countExtra == null) {
			stopSelf();
			return;
		}
		
		if(count != null) {
			sendKillBroadcast(count);
		}
		
		play(countExtra);
		count = countExtra;
		
		initialCallState = phoneManager.getCallState();
	}
	
	private void sendKillBroadcast(CountDown c) {
		Intent alarmKilled = new Intent(CountDowns.ALARM_KILLED);
		alarmKilled.putExtra(CountDowns.COUNTDOWN_INTENT_EXTRA, c);
		sendBroadcast(alarmKilled);
	}
	
	private static final float IN_CALL_VOLUME = 0.125f;
	
	private void play(CountDown c) {
		stop();
		
		Uri alert = null;
		alert = Uri.parse(prefs.getString(SupaCountPreferences.KEY_RINGTONE, 
				SupaCountPreferences.DEFAULT_ALARM));
		if(DEBUG) Log.d(TAG, "alert: " + alert);
		
		player = new MediaPlayer();
		player.setOnErrorListener(new OnErrorListener() {
			public boolean onError(MediaPlayer mp, int what, int extra) {
				if(DEBUG) Log.e(TAG, "Error occurred while playing audio.");
				mp.stop();
				mp.release();
				player = null;
				return true;
			}
		});
		
		try {
			if(phoneManager.getCallState() != TelephonyManager.CALL_STATE_IDLE)
			{
				if(DEBUG) Log.v(TAG, "Using in-call alarm");
				player.setVolume(IN_CALL_VOLUME, IN_CALL_VOLUME);
				setDataSourceFromResource(getResources(), player, 
						R.raw.in_call_alarm);
			}
			else {
				player.setDataSource(this, alert);
			}
			startAlarm(player);
		}
		catch(Exception ex) {
			if(DEBUG) Log.d(TAG, "Failed to play ringtone", ex);
		}
		
		if(prefs.getBoolean(SupaCountPreferences.KEY_VIBRATE, 
				SupaCountPreferences.DEFAULT_VIBRATE)) { 
			vibrator.vibrate(vibratePattern, 0);
		}
		
		enableKiller(c);
		playing = true;
	}
	
	private void startAlarm(MediaPlayer player) throws java.io.IOException,
		IllegalArgumentException, IllegalStateException {
		player.setAudioStreamType(AudioManager.STREAM_ALARM);
		player.setLooping(true);
		player.prepare();
		player.start();
	}
	
	private void setDataSourceFromResource(Resources resources, 
			MediaPlayer player, int res) 
			throws java.io.IOException {
		AssetFileDescriptor afd = resources.openRawResourceFd(res);
		if(afd != null) {
			player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
					afd.getLength());
			afd.close();
		}
	}
	
	/**
	 * Stops alarm audio
	 */
	public void stop() {
		if(playing) {
			playing = false;
			
			if(player != null) {
				player.stop();
				player.release();
				player = null;
			}
			
			vibrator.cancel();
		}
		
		disableKiller();
	}
	
	private void enableKiller(CountDown c) {
		handler.sendMessageDelayed(handler.obtainMessage(KILLER, c),
				1000 * ALARM_TIMEOUT_SECONDS);
	}
	
	private void disableKiller() {
		handler.removeMessages(KILLER);
	}
}
