package org.vimtips.supacount;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Receives the Alarm broadcast from the AlarmManager when a CountDown 
 * completes.
 * 
 * This code is based on, if not entirely copied from, Android's 
 * stock AlarmClock application.  Code for their AlarmClock application can
 * be located at this address: http://android.git.kernel.org/  
 * 
 * @author Adam Olsen
 *
 */
public class AlarmReceiver extends BroadcastReceiver {
	private SharedPreferences prefs;

	/**
	 * Called by the AlarmManager when a timeout occurs
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		if(prefs == null) {
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
		}
		if(CountDowns.CLEAR_NOTIFICATION.equals(intent.getAction())) {
			context.stopService(new Intent(CountDowns.COUNTDOWN_ALERT_ACTION));
			return;
		}
		else if(CountDowns.ALARM_KILLED.equals(intent.getAction())) {
			updateNotification(context, 
				(CountDown)intent.getParcelableExtra(CountDowns.COUNTDOWN_INTENT_EXTRA));
			return;
		}
		
		CountDown count = null;
		
		// Grab the CountDown from the Intent.  Since the AlarmManager
		// fill in the Intent to add some extra data, it must unparcel the 
		// CountDown object.  It throws ClassNotFoundException when unparcelling, 
		// so to avoid this, we do the unmarshalling ourselves.
		final byte[] data = intent.getByteArrayExtra(CountDowns.COUNTDOWN_RAW_DATA);
		if(data != null) {
			Parcel in = Parcel.obtain();
			in.unmarshall(data, 0, data.length);
			in.setDataPosition(0);
			count = CountDown.CREATOR.createFromParcel(in);
		}
		
		if(count == null) {
			Log.v("AlarmReceiver", "Failed to parse countdown from intent.");
			return;
		}
		
		AlarmAlertWakeLock.acquireCpuWakeLock(context);
		
		Intent closeDialogs = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
		context.sendBroadcast(closeDialogs);
		
		@SuppressWarnings("unchecked")
		Class c = AlarmAlert.class;
		KeyguardManager km = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
		if(km.inKeyguardRestrictedInputMode()) {
			c = AlarmAlertFullScreen.class;
		}
		
		Intent alarmAlert = new Intent(context, c);
		alarmAlert.putExtra(CountDowns.COUNTDOWN_INTENT_EXTRA, count);
		alarmAlert.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_USER_ACTION);
		context.startActivity(alarmAlert);

		// resets this countdown, sets up the next scheduled alarm
		count.reset();
		CountDowns.saveCountDown(context, count);
		CountDowns.scheduleNextAlarm(context);
		
		// play the alarm and vibrate the device
		Intent playAlarm = new Intent(CountDowns.COUNTDOWN_ALERT_ACTION);
		playAlarm.putExtra(CountDowns.COUNTDOWN_INTENT_EXTRA, count);
		context.startService(playAlarm);
		
		Intent notify = new Intent(context, AlarmAlert.class);
		notify.putExtra(CountDowns.COUNTDOWN_INTENT_EXTRA, count);
		
		PendingIntent pendingNotify = PendingIntent.getActivity(context, 
				count.getId(), notify, 0);
		
		String label = count.getDescription();
		Notification n = new Notification(R.drawable.icon, 
				count.getDescription(), System.currentTimeMillis());
		n.setLatestEventInfo(context, label, 
				context.getString(R.string.count_complete), 
				pendingNotify);
		
		/**
		 * Check to see if the user wants to blink the LED
		 */
		if(prefs.getBoolean(SupaCountPreferences.KEY_LED, 
				SupaCountPreferences.DEFAULT_LED)) {
			n.flags |= Notification.FLAG_SHOW_LIGHTS;
			n.ledARGB = 0xFF00FF00;
			n.ledOnMS = 500;
			n.ledOffMS = 500;
		}
		
		Intent clearAll = new Intent(context, AlarmReceiver.class);
		clearAll.setAction(CountDowns.CLEAR_NOTIFICATION);
		n.deleteIntent = PendingIntent.getBroadcast(context, 0, clearAll, 0);
		
		NotificationManager nm = (NotificationManager)context.getSystemService(
				Context.NOTIFICATION_SERVICE);
		nm.notify((int)count.getId(), n);
	}
	
	/**
	 * Updates the notification to be a silent notification if the alarm has been
	 * playing for too long (AlarmService.ALARM_TIMEOUT_SECONDS).  This saves battery.
	 * @param context
	 * @param count
	 */
	public void updateNotification(Context context, CountDown count) {
		NotificationManager nm = 
			(NotificationManager)context.getSystemService(
					Context.NOTIFICATION_SERVICE);
		if(count == null) {
			return;
		}
		
		Intent viewAlarm = new Intent(context, SupaCount.class);
		viewAlarm.putExtra(CountDowns.COUNTDOWN_ID, count.getId());
		
		PendingIntent intent = PendingIntent.getActivity(context, 
				count.getId(), viewAlarm, 0);
		String label = count.getDescription();
		Notification n = new Notification(R.drawable.clock, 
				label, count.getTimeStarted());
		n.setLatestEventInfo(context, label, 
				context.getString(R.string.count_complete), intent);
		n.flags |= Notification.FLAG_AUTO_CANCEL;
		nm.notify(count.getId(), n);
	}
}